# wordpress-example

This project contains an example of how to create DO droplet and put `wp` folder, thhen build image from Dockerfile and start services with `docker-compose`. Everything is done using [terraform](https://www.terraform.io/)

## Prerequisites

To use this terraform config you should:

* Fork this project into your private repo
* Change wordpress image name in `wp/docker-compose.yml`
* Install [terraform](https://www.terraform.io/downloads.html)
* Create auth token in you DO account and place it into `terraform.tfars` file:
  ```
  do_token="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  ```

## Usage

This terraform config creates DigitalOcean droplet with ubuntu, installs docker, copies wp folder, builds wp image and runs docker-compose to start mysql and wordpress containers.
As an output it gives you an ip-address of your wordpress droplet.
To create droplet and wordpress service run:

```
$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  + digitalocean_droplet.main
      id:                         <computed>
      backups:                    "false"
      disk:                       <computed>
      image:                      "ubuntu-18-04-x64"
      ipv4_address:               <computed>
      ipv4_address_private:       <computed>
      ipv6:                       "false"
      ipv6_address:               <computed>
      ipv6_address_private:       <computed>
      locked:                     <computed>
      memory:                     <computed>
      monitoring:                 "false"
      name:                       "wp"
      price_hourly:               <computed>
      price_monthly:              <computed>
      private_networking:         "true"
      region:                     "lon1"
      resize_disk:                "true"
      size:                       "512mb"
      ssh_keys.#:                 <computed>
      status:                     <computed>
      vcpus:                      <computed>
      volume_ids.#:               <computed>

  + digitalocean_ssh_key.main
      id:                         <computed>
      fingerprint:                <computed>
      name:                       "Terraform Wordpress Example"
      public_key:                 "${tls_private_key.main.public_key_openssh}"

  + tls_private_key.main
      id:                         <computed>
      algorithm:                  "RSA"
      ecdsa_curve:                "P224"
      private_key_pem:            <computed>
      public_key_fingerprint_md5: <computed>
      public_key_openssh:         <computed>
      public_key_pem:             <computed>
      rsa_bits:                   "2048"


Plan: 3 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes
```

After typing yes, terraform will create everything you need to run droplet and wordpress.
After successfull run you'll get:

```
Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

Outputs:

wp_address = 209.97.130.7
```

To get ssh private key, run:

```
$ terraform state show tls_private_key.main
```

To enable auto deploy, just add to the settings of your CI/CD two variables:

```
DEPLOYMENT_SERVER_IP = <your-wp-ip-address>
DEPLOY_SERVER_PRIVATE_KEY = <tls-private-key>
```
